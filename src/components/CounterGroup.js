import Counter from "./Counter";

const CounterGroup = ({ onChange, counterList }) => {

    const handleChange = (index, value) => {
        const updateConterList = [...counterList];
        updateConterList[index] = value;
        onChange(updateConterList);
    }


    return (
        <div>
            {
                counterList.map((value, index) => 
                <Counter index={index} value={value} onChange={handleChange}></Counter>)
            }
        </div>

    )
}


export default CounterGroup