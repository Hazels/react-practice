import CounterGroup from './CounterGroup';
import CounterGroupSum from './CounterGroupSum';
import CounterSizeGenerator from './CounterSizeGenerator';
import { useState } from 'react';

const MultipleCounter = () => {
    // const [counterSize, setCounterSize] = useState(0);
    // const handleChangeCounterSize = (updateSize) => {
    //     setCounterSize(updateSize);
    // }
    const [counterList, setCounterList] = useState([]);
    const handleChangeCounterList = (size) => {
        setCounterList(Array(size).fill(0));
    }
    const sum = counterList.reduce((counter, number) => counter + number, 0);
    return (
        <div>
            <CounterSizeGenerator
                size={counterList.length}
                onChange={handleChangeCounterList}></CounterSizeGenerator>
            <CounterGroupSum sum={sum}></CounterGroupSum>
            <CounterGroup counterList={counterList} onChange={setCounterList} ></CounterGroup>
        </div>
    )
}

export default MultipleCounter