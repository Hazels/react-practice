
const Counter = ({onChange, index, value}) => {
    // const [count, setCount] = useState(0);
    const handleIncrease = () => {
        onChange(index, value + 1)
        // setCount(count + 1);
    }
    const handleDecrease = () => {
        onChange(index, value - 1)
        // setCount(count - 1);
    }
    return (
        <div>
            <button onClick={handleIncrease}>+</button>
            <span>{value}</span>
            <button onClick={handleDecrease}>-</button>
        </div>
    )
}


export default Counter