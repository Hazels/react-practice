
const CounterSizeGenerator = ({onChange,size}) => {

    const handleChange = (event) => {

        onChange(Number(event.target.value))
    }

    return (
        <div>
            size: <input onChange={handleChange} type="number" value={size}></input>
        </div>
    )
}


export default CounterSizeGenerator